# Campflix Movie Website

Campflix a simple movie website that can give you a best recomendation movie to watch. This website is created as a assignment technical test from Campaign company.

MVP Requirement from campaign:
1. Show name and title atribute from the API that have been given by company
2. make a searching functions
3. Display movies on horizontal scroll

Tech that have been used:
1. ReactJs
2. Firebase ( for the Login/Register API )
3. Redux
4. Mat Ui

Other libary :
1. jwt-decode (for decode token)
2. redux-thunk (middleware that will give us dispatch to run asycronus code in redux)
3. React-responsive

Feautes so far :
1. Login (Already authenticated with firebase)
2. Register
3. Display list of movies

Responsive website : 60% (still in the progress)

