export default {
    palette:{
        common:{
          black:'#1E1E1E',// black
          white:'#FFFFFF',// white
          red: '#f44336', //RED #1E1E1E
          orange: '#FF704D', // Orange figma
          whiteButtonBackground: '#F3F3F3',
          background: "-webkit-linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",

        },
        primary:{
          light:'#1E1E1E', //Black
          main:'#ff9e80',
          dark:'#1E1E1E',
          contrastText:'#fff'
        },
        secondary:{
          light:'#42C38B',//Light Green
          main:'#2BB16A',//Green from figma
          dark:'#199946',// Dark Green
          contastText:'#fff'
        }
      },
      breakpoints: {
        values: {
          xs: 0,
          sm: 640,
          md: 767,
          lg: 1024,
          xl: 1280,
          fullHD: 1536
        },
      }
}