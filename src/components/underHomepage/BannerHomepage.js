import React from 'react';
import homeBanner from '../../images/campaign.png';

//MuI Stuff
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme=>({
    contBanner:{
        //mengatur posisi, lebar, tinggi, kotak hitam
        position: 'relative',
        width: '100%',
        height: '25vw',
        backgroundColor: 'rgb(0,0,0,0.1)',

        //mengatur flex dan padding terhadap gambar
        paddingTop: 130,
        textAlign: 'center',
        display: 'flex',
        justifyContent : 'center',
        [theme.breakpoints.down('sm')]: {
            height: '40vw',
            paddingTop: 90,
        }
    },
    imgBanner:{
        position: 'absolute',
        width: '87vw',
        height: '21vw',
        [theme.breakpoints.down('sm')]: {
            height: '32vw',
        }
    },
    h1Style:{
        position: 'absolute',
        paddingTop: 25,
        fontFamily: 'Poppins',
        fontSize: 48,
        fontWeight: 600,
        [theme.breakpoints.down('sm')]: {
            paddingTop: 8,
            fontSize: 20,
        }
    },
    slogan:{
        position: 'absolute',
        paddingTop :95,
        [theme.breakpoints.down('sm')]: {
            paddingTop: 35,
            fontSize: 10,
        }
    }
}))

const BannerHomepage = () => {

    const classes = useStyles();
    return ( 
        <div>
            <div className={classes.contBanner}>
                <img src={homeBanner} alt="homepage banner" className={classes.imgBanner}/>
                <h1 className={classes.h1Style}>Welcome to Campflix</h1>
                <p className={classes.slogan}>Connecting your cinema to home</p>
                {/**di sini isi komponen search box nya */}
            </div>
        </div>
     );
}
 
export default BannerHomepage;