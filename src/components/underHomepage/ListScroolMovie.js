import React from 'react';
import noimg2 from '../../images/noimg2.png';
import '../../css/scrollbar.css'

//MaT UI Stuff
import { makeStyles } from '@material-ui/core/styles';

//Redux Stuff
import {  useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
    card:{
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: 'black',
        textAlign: 'center',
        paddingBottom: 30,
        width: '18vw',
        height: '19vw',
        borderRadius: '2%',
        [theme.breakpoints.down('sm')]: {
            width: '18vw',
            height: '19vw',
        }
    },
    imaged:{
        // position: 'absolute',
        width: '75%',
        height: '85%',
        border: '1px solid orange',
    },
    noImaged:{
        // position: 'absolute',
        width: '75%',
        height: '85%',
    },
    contForFlexWrap:{
        flexWrap: 'nowrap',
        margin:'2%'
    },
    contList:{
        justifyContent: 'justify',
        alignItems: 'center',
        display: 'flex',
        marginTop: 20,
        marginLeft: '5%',
        marginRight: '5%',
        marginBottom: 20,
        overflowX: 'scroll',
        overflowY: 'hidden',
        width: '88%',
        height: '26vw',
        backgroundColor: 'rgb(0,0,0,0.1)',
        [theme.breakpoints.down('sm')]: {
            width: '90%',
            height: '35vw',
        }
    },
    titleMovie:{
        position: 'absolute',
        fontSize: 20,
        fontWeight: 600,
        top: '80%',
        left: '10%',
        right: '10%',
        color:'white',
        [theme.breakpoints.down('sm')]: {
            fontSize: 10,
            top: '60%',
        }
    }
  }));

const ListScroolMovie = () => {

    const classes = useStyles();
    const {dataMovies, textInSearchBox} = useSelector (state => state.data);

    const filterDataMovies = dataMovies.filter((dataMov)=>{
        return dataMov
            .show.name.toLocaleLowerCase().includes(textInSearchBox.toLocaleLowerCase())
    })

    let contentGridList = filterDataMovies.map((movie)=>{

        let pic = movie.show.image

        return (
            <div className={classes.contForFlexWrap} key={movie.show.id}>
                <div className={classes.card}  >
                    {
                        pic !== undefined && pic !== null ? (
                            <img src={`${pic.original}`} alt="Movie" className={classes.imaged}/>
                            ) : (
                            <img src={noimg2} alt="No Image" className={classes.noImaged}/>
                            )
                    }
                      
                      <p className={classes.titleMovie}>{movie.show.name}</p>

                </div>
            </div>
        )
    })

    return ( 
         <div className={classes.contList}> 
            {contentGridList} 
        </div>  
         
     );
}
 
export default ListScroolMovie;