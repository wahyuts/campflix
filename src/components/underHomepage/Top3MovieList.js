import React from 'react';
import noimg from '../../images/noimg.png';
import '../../css/scrolbarY.css'

//MuI Stuff
import { makeStyles } from '@material-ui/core/styles';

//Redux Stuff
import {useSelector} from 'react-redux';

const useStyles = makeStyles((theme)=>({
    contCardList:{
        display: 'flex',
        justifyContent: 'justify',
        alignItems: 'center',
        backgroundColor:'rgb(0,0,0,0.1)',
        width: '88%',
        height: '22vw',
        marginLeft: '5%',
        marginRight: '5%',
        marginBottom: 20,
        borderRadius: 10
    },
    contCardPic:{
        position: 'relative',
        backgroundColor: 'black',
        width: '17vw',
        height: '19vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '2%',
        borderRadius: 5
    },
    contCardText:{
        position: 'relative',
        backgroundColor: 'rgb(0,0,0,0.1)',
        width: '100%',
        height: '19vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '2%',
        marginRight: '2%',
        paddingTop: '2%',
        paddingLeft: '2%',
        paddingRight: '2%',
        borderRadius: '2%',
        overflowY: 'scroll',
        overflowX: 'hidden',
        [theme.breakpoints.down('sm')]: {
            fontSize: 10,
        }
    },
    h3text: {
        textAlign: 'center',
        fontSize: 40,
        [theme.breakpoints.down('sm')]: {
            fontSize: 25,
        }
    },
    contImage:{
        position: 'absolute',
        width: '75%',
        height: '85%',
        border: '1px solid orange'
    },
}))

const Top3MovieList = () => {
    const classes = useStyles()
    const {dataMovies, loading} = useSelector (state => state.data);

    const newSliceMovies = dataMovies.slice(1,4);
    console.log('array slice', newSliceMovies)

    let top3Movies = newSliceMovies.map((top3Mov,i)=>{
        let pic = top3Mov.show.image
        let desc = top3Mov.show.summary
        return(
            <div key={i} >
                <div className={classes.contCardList}>
                    <div className={classes.contCardPic} >
                        {
                            pic !== undefined && pic !== null ? (
                                <img src={`${pic.original}`} alt="Movie" className={classes.contImage}/>
                                ) : (
                                <img src={noimg} alt="No Image" className={classes.contImage}/>
                                )
                        }
                    </div>
                    <div className={classes.contCardText}>
                        {
                            desc !== undefined && desc !== null ? (
                                <p>{desc}</p>
                            ):(
                                <p>Deskripsi belum tersedia</p>
                            )
                        }
                    </div>
                </div>
                {/* <div className={classes.contRight}>
                    <div className={classes.contEachRight}>
                        <img className={classes.image}/>
                        <p></p>
                    </div>
                </div>
                <div className={classes.contLeft}>
                    <div className={classes.contEachLeft}>
                        <img className={classes.image}/>
                        <p></p>
                    </div>
                </div> */}
            </div>
        )
    })

    return ( 
        <div>
            <div className={classes.h3text}>
                <h3>Recomendation Movie</h3>
            </div>
            {top3Movies}
        </div>
     );
}
 
export default Top3MovieList;