import React from 'react';
import {Link, useHistory} from 'react-router-dom';
import SearchBox from '../../components/SearchBox';
import {Desktop,Tablet,Mobile} from '../../util/ReactResponsiveHook';

//MaT UI Stuff
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

//Redux
import {useDispatch,useSelector} from 'react-redux';
import {logoutUser} from '../../redux/actions/userActions';

//Icons
import AccountCircle from '@material-ui/icons/AccountCircle';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles(theme=>({
    toolbar:{
        // position: 'relative',
        height: 85
    },
    nameLogo:{
        marginLeft: '5%',
        color: theme.palette.common.white,
        [theme.breakpoints.down('sm')]: {
            fontSize:20,
            marginBottom:15
        }
    },
    image:{
        marginLeft: '10%',
        [theme.breakpoints.down('sm')]: {
            marginRight: 'auto',
            marginLeft: '5%',
            height:40,
            width:130
        }
    },
    buttonDiv:{
        display: 'flex',
        width: '90%',
        justifyContent:'flex-end',
        alignItems:'center',
        marginLeft: 'auto',
        marginRight: '5%',
        // backgroundColor: 'green',
        [theme.breakpoints.down('sm')]: {
            marginRight: '5%'
        },
        '& .signUpButton':{
            height: 50,
            width: 140,
            textTransform: 'none',
            backgroundColor: theme.palette.common.whiteButtonBackground,
            color: theme.palette.common.black,
            [theme.breakpoints.down('sm')]: {
                height:30,
                width:70,
                fontSize:10
            }
        },
        '& .signInButton':{
            textTransform: 'none',
            color: theme.palette.common.white,
            marginRight: 20
        }
    },
    menuBurgerAndSbox:{
        display:'flex',
        width: '90%',
        justifyContent:'flex-end',
        alignItems:'center',
        color: 'white',
        marginRight: '5%',
        [theme.breakpoints.down('sm')]: {
            width: '70%',
        }
    },
}))


const HeadNav = () => {
    const classes= useStyles();
    const dispatch = useDispatch();
    // const history = useHistory();

    const {authenticated,loading, credentials: {name}} = useSelector (state => state.user);

    const logout = () => {
        dispatch(logoutUser());
    }

    let buttonTB= !loading ? (authenticated ? (
        <div className={classes.buttonDiv}>
                    <SearchBox/>
                    <AccountCircle style={{marginRight:5, color: 'white'}}/>
                    <p style={{color:'white',marginRight:5}}>{name}</p> 
                    <LogoutIcon style={{marginLeft:5, color: 'white', fontSize:30}} onClick={logout}/>
        </div>
    ) : (
        <div className={classes.buttonDiv}>
                    <SearchBox/>
                    <Button className="signInButton" component={Link} to='/login'>
                        <AccountCircle style={{marginRight:5}}/> Log In
                    </Button>
                    <Button className="signUpButton" component={Link} to='/signup'>
                        Sign up free <ArrowForwardIcon style={{marginLeft:5}}/>
                    </Button>
        </div>
    )) : (<p style={{fontSize:20, color:'white', marginRight: '5%', marginLeft:'auto'}}>...Loading</p>)


    let menuBurgerAndSearchBox = (
        <div className={classes.menuBurgerAndSbox}>
            <SearchBox/>
            <MenuIcon className={classes.menuIcon}/>
        </div>
    )

    return ( 
        <AppBar color="transparent" position="absolute" style={{boxShadow:'none'}} >
            <Toolbar className={classes.toolbar} >
                <h1 className={classes.nameLogo}>Campflix</h1>
                <Mobile>
                    {menuBurgerAndSearchBox}
                </Mobile>
                <Tablet>
                    {buttonTB}
                </Tablet>
                <Desktop>
                    {buttonTB}
                </Desktop>
            </Toolbar>
        </AppBar>
     );
}
 
export default HeadNav;