import React,{useEffect} from 'react';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import { useHistory } from "react-router-dom";
import CircularProgress from '@material-ui/core/CircularProgress';
import '../css/LoginComp.css';

//Redux stuff
import { useDispatch, useSelector } from "react-redux";
import {signupUser} from '../redux/actions/userActions';

const SignupComp = () => {
    const {loading} = useSelector (state => state.UI);
    const {errors} = useSelector (state => state.UI);
    const dispatch = useDispatch();
    const History = useHistory();

    const formik = useFormik (
        {
            initialValues: {
                email: "",
                password: ""
            },
            validationSchema: Yup.object({
                name: Yup.string()
                    .min(2, "Mininum 2 characters")
                    .max(20, "Maximum 20 characters")
                    .required("Required!"),
                email: Yup.string()
                    .email("Invalid email format")
                    .required("Required!"),
                password: Yup.string()
                    .min(6, "Minimum 6 characters")
                    .required("Required!"),
                confirmPassword: Yup.string()
                    .min(6, "Minimum 6 characters")
                    .required("Required!")
            }),

            onSubmit: values => { 
                dispatch(signupUser(values, History));
              }
        }
    )

    
    useEffect(()=>{
        if (errors !== undefined && errors !== null ) {
            History.push("/");
        } else {
            return false
        }
    },[])


    return ( 
        <div>
            <form className="contForm" onSubmit={formik.handleSubmit}>
                <div className="divForm">
                    <label className="labelForm">Name</label>
                    <input 
                        type="text" 
                        name="name" 
                        value={formik.values.full_name}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.full_name && formik.touched.full_name && (
                            <p className="errorMsg">{formik.errors.full_name}</p>
                        )}
                </div>
                <div className="divForm">
                    <label className="labelForm">Email</label>
                    <input 
                        type="email" 
                        name="email" 
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.email && formik.touched.email && (
                            <p className="errorMsg">{formik.errors.email}</p>
                        )}
                </div>
                <div className="divForm">
                    <label className="labelForm">Password</label>
                    <input 
                        type="password" 
                        name="password" 
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.password && formik.touched.password && (
                            <p className="errorMsg">{formik.errors.password}</p>
                        )}
                </div>
                <div className="divForm">
                    <label className="labelForm">Password</label>
                    <input 
                        type="password" 
                        name="confirmPassword" 
                        value={formik.values.confirmPassword}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.confirmPassword && formik.touched.confirmPassword && (
                            <p className="errorMsg">{formik.errors.confirmPassword}</p>
                        )}
                </div>
                
                <div className="divForm">
                    <button type="submit" className="buttonForm">
                        Register
                        {loading && ( 
                                    <CircularProgress className="progress" size={18}/>
                                )}
                    </button>
                    {errors && ( 
                                <p className="customError">
                                    {errors.general}
                                </p>
                            )}
                </div>
                <div className="flex-text">
                    <p style={{marginRight:10}}>Already have an account?</p>
                    <p className="regHover" onClick={()=>History.push("/login")}>SignIn here</p>
                </div>
            </form>
        </div>
     );
}
 
export default SignupComp;