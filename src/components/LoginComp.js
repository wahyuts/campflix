import React,{useEffect} from 'react';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import { useHistory } from "react-router-dom";
import '../css/LoginComp.css';
import CircularProgress from '@material-ui/core/CircularProgress';

//Redux stuff
import { useDispatch, useSelector } from "react-redux";
import {loginUser} from '../redux/actions/userActions';

const LoginComp = (props) => {
    const {loading} = useSelector (state => state.UI);
    const {errors} = useSelector (state => state.UI);

    const dispatch = useDispatch();
    const History = useHistory();

    const formik = useFormik (
        {
            initialValues: {
                email: "",
                password: ""
            },
            validationSchema: Yup.object({
                email: Yup.string()
                    .email("Invalid email format")
                    .required("Required!"),
                password: Yup.string()
                    .min(6, "Minimum 6 characters")
                    .required("Required!")
            }),

            onSubmit: values => { 
                dispatch(loginUser(values, History));
            }
        }
    )

    useEffect(()=>{
        if (errors !== undefined && errors !== null ) {
            History.push("/");
        } else {
            return false
        }
    },[])

    console.log("error", errors)
    return ( 
        <div>
            <form className="contForm" onSubmit={formik.handleSubmit}>
                <div className="divForm">
                    <label className="labelForm">Email</label>
                    <input 
                        type="email" 
                        name="email" 
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.email && formik.touched.email && (
                            <p className="errorMsg">{formik.errors.email}</p>
                        )}
                </div>
                <div className="divForm">
                    <label className="labelForm">Password</label>
                    <input 
                        type="password" 
                        name="password" 
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        className="inputField" />

                        {formik.errors.password && formik.touched.password && (
                            <p className="errorMsg">{formik.errors.password}</p>
                        )}
                </div>
                <div className="text-forget-password">
                    <p className="cursorText">Lupa Password ?</p>
                    <p style={{marginLeft:10,marginRight:10}}>|</p>
                    <p className="cursorText">Konfirmasi Email?</p>
                </div>
                <div className="divForm">
                    <button type="submit" disabled={loading} className="buttonForm">
                        Login
                        {loading && ( // jika loading true maka tampilkan spinner
                                    <CircularProgress className="progress" size={18}/>
                                )}
                    </button>
                    {errors && ( 
                                <p className="customError">
                                    {errors.general}
                                </p>
                            )}
                </div>
                <div className="flex-text">
                    <p style={{marginRight:10}}>Don't have an account?</p>
                    <p className="regHover" onClick={()=>History.push("/signup")}>Register now</p>
                </div>
            </form>
        </div>
     );
}
 
export default LoginComp;