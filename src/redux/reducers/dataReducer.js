import {LOADING_DATA, GET_DATA_MOVIE, SET_TEXT_FOR_SEARCHING} from '../type';

const initialState = {
    dataMovies: [],
    textInSearchBox: '',
    loading: false
}

export default function (state=initialState, action) {
    switch (action.type){
        case LOADING_DATA:
            return{
                ...state,
                loading:true
            }
        case GET_DATA_MOVIE:
            return{
                ...state,
                dataMovies: action.payload,
                loading: false
            }
        case SET_TEXT_FOR_SEARCHING:
            return{
                ...state,
                textInSearchBox: action.payload
            }
        default:
            return state;
    }
}