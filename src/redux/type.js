//user reducer types
export const SET_AUTHENTICATED = 'SET_AUTHENTICATED';
export const SET_UNAUTHENTICATED = 'SET_UNAUTHENTICATED';
export const SET_USER = 'SET_USER';
export const LOADING_USER = 'LOADING_USER';

//data reducer types
export const GET_DATA_MOVIE ='GET_DATA_MOVIE';
export const SET_TEXT_FOR_SEARCHING = 'SET_TEXT_FOR_SEARCHING';

//ui reducer types
export const SET_ERRORS='SET_ERRORS';
export const CLEAR_ERRORS='CLEAR_ERRORS';
export const LOADING_UI = 'LOADING_UI';
export const LOADING_DATA = 'LOADING_DATA';
export const STOP_LOADING_UI = 'STOP_LOADING_UI';

