import React,{useEffect} from 'react';
import BannerHomePage from '../components/underHomepage/BannerHomepage';
import ListScroolMovie from '../components/underHomepage/ListScroolMovie';
import Top3MovieList from '../components/underHomepage/Top3MovieList';

//MaT UI Stufff
import { makeStyles } from '@material-ui/core/styles';

//Redux Stuff
import { useDispatch } from "react-redux";
import {getDataMovie} from '../redux/actions/dataActions';


const useStyles = makeStyles((theme) => ({
    h3text: {
      textAlign: 'center',
      marginTop: 50,
      marginBottom: 0,
      fontSize: 40,
      [theme.breakpoints.down('sm')]: {
        fontSize: 25,
    },
    },
    
  }));

const HomePage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    useEffect(()=>{
      dispatch(getDataMovie())
    })
    
    return ( 
        <div>
            <BannerHomePage/>
            <h3 className={classes.h3text}>List Movie</h3>
            <div style={{display: 'flex', justifyContent: 'center'}}>
                <ListScroolMovie/>
            </div>
            <div>
                <Top3MovieList/>
            </div>
        </div>
     );
}
 
export default HomePage;