import React from 'react';
import SignupComp from '../components/SignupComp.js';
import FooterSignInUp from '../components/FooterSignInUp';
import '../css/LoginPage.css';


const SignupPage = () => {
    return ( 
        <div className="bck">

            <div className="only-width-height-signUpComp">
                <h1 className="titleSignup">Campflix</h1>
                <SignupComp/>
            </div>
            {/* <div className="only-width-height-footer">
                <FooterSignInUp/>
            </div> */}
        </div>
     );
}
 
export default SignupPage;